require "rspec"
require_relative '../lib/nrepl-lazuli'
require_relative '../lib/nrepl-lazuli/client'

RSpec.describe NREPL::Server do
  port = 0
  let(:server) { NREPL::Server.spawn(port: 3030 + port, pwd: "/tmp") }
  let(:client) do
    server
    NREPL::Client.new(port: 3030 + port)
  end

  before do
    port += 1
  end

  after do
    server.stop
    client.stop
  end

  context 'with tracing disabled' do
    let(:server) { NREPL::Server.spawn(port: 3030 + port, tracing: false) }

    it "evaluates code" do
      eval_msg = {
        'op' => 'eval',
        'code' => ":hello",
        'id' => 'some_id'
      }
      ret = client.rpc(eval_msg)
      expect(ret).to eql({
        "id"=>"some_id",
        "session"=>"none",
        "status"=>["done"],
        "value"=>":hello"
      })

       # Evaluates at the right row
      eval_msg = {
        'op' => 'eval',
        'code' => <<-RUBY,
class Foo
  def bar
  end
end
Foo.instance_method(:bar).source_location
        RUBY
        'id' => 'method',
        'line' => 0,
        'file' => 'some_file.rb'
      }
      ret = client.rpc(eval_msg)
      expect(ret).to eql(
        "id"=>"method",
        "session"=>"none",
        "status"=>["done"],
        "value"=>'["some_file.rb", 2]'
      )
    end

    it "binds variables manually with NREPL.watch!" do
      res = define_watch_point!(client)
      expect(res).to eql({
        "id"=>"eval_watch",
        "value"=>":stopped_call",
        "session"=>"none",
        "status"=>["done"]
      })

      first_watch, second_watch = eval_to_watch_method!(client)
      expect(first_watch).to eql({
        "op"=>"hit_watch",
        "file" => "/tmp/some_file.rb",
        "line" => 22,
        'id' => 'some-id',
        'status' => ['done']
      })
      expect(second_watch).to eql({
        "op"=>"hit_watch",
        "file" => "/tmp/some_file.rb",
        "line" => 23,
        'id' => 'after-method',
        'status' => ['done']
      })

      # Then it'll get the result
      expect(client.read).to eql({
        "id"=>"eval_to_watch",
        "session"=>"none",
        "value" => "42",
        "status"=>["done"]
      })

      # It can get the watches for the file
      res = client.rpc('op' => 'get_watches', 'file' => '/tmp/some_file.rb')
      expect(res['watches']).to eql([
        {'file' => '/tmp/some_file.rb', 'line' => 22, 'id' => 'some-id'},
        {'file' => '/tmp/some_file.rb', 'line' => 23, 'id' => 'after-method'}
      ])

      # Finally, it can evaluate under that id
      eval_msg = {
        'id' => 'eval_1',
        'op' => 'eval_pause',
        'code' => 'variable += 1',
        'watch_id' => 'some-id',
        "file" => "/tmp/some_file.rb",
        "line" => 20
      }
      res = client.rpc(eval_msg)
      expect(res).to eql({
        "id"=>"eval_1",
        "session"=>"none",
        "value" => "41",
        "status"=>["done"]
      })

      # And it'll keep the binding
      eval_msg.update('code' => 'variable', 'id' => 'eval_2', 'watch_id' => 'some-id')
      expect(client.rpc(eval_msg)).to eql({
        "id"=>"eval_2",
        "session"=>"none",
        "value" => "41",
        "status"=>["done"]
      })

      # Finally, it can unwatch
      res = client
        .rpc('op' => 'unwatch', 'watch_id' => 'some-id')
        .tap { |x| x.delete('id') }
      # res = client.read!
      expect(res).to eql({ 'op' => 'unwatch', 'session' => 'none', "status"=>["done"] })

      # And the binding is gone
      eval_msg.update('code' => 'variable', 'id' => 'eval_3', 'watch_id' => 'some-id')
      expect(client.rpc(eval_msg)['ex']).to match(/undefined local variable or method `variable'/)
    end

    it "updates watch points based on changes at the editor" do
      res = define_watch_point!(client)
      eval_to_watch_method!(client)
      # Get the result
      client.read

      msg = {
        'op' => 'update_watch_lines',
        'file' => '/tmp/some_file.rb',
        'line' => 22,
        'delta' => 2
      }
      expect(client.rpc(msg)['watches']).to eql([
        {'file' => '/tmp/some_file.rb', 'line' => 22, 'id' => 'some-id'},
        {'file' => '/tmp/some_file.rb', 'line' => 25, 'id' => 'after-method'}
      ])
      expect(client.rpc(msg.merge('line' => 21, 'delta' => -20))['watches']).to eql([
        {'file' => '/tmp/some_file.rb', 'line' => 2, 'id' => 'some-id'},
        {'file' => '/tmp/some_file.rb', 'line' => 5, 'id' => 'after-method'}
      ])
    end

    it "updates watch points and rename their IDs to match the new row" do
      res = define_watch_point!(client, default_ids: true)
      eval_to_watch_method!(client)
      # Get the result
      client.read

      msg = {
        'op' => 'update_watch_lines',
        'file' => '/tmp/some_file.rb',
        'line' => 22,
        'delta' => 2
      }
      expect(client.rpc(msg)['watches']).to eql([
        {'file' => '/tmp/some_file.rb', 'line' => 22, 'id' => '/tmp/some_file.rb:23'},
        {'file' => '/tmp/some_file.rb', 'line' => 25, 'id' => '/tmp/some_file.rb:26'}
      ])
    end

    it "breaks eval" do
      msg = { 'id' => 'e1', 'op' => 'eval', 'file' => '/tmp/ex.rb', 'line' => 0, 'code' => <<-RUBY }
      sleep 10
      39
      RUBY

      result = client.write(msg)
      client.write({'interrupt-id' => 'e1', 'op' => 'interrupt'})
      expect(client.read).to eql(
        'id' => 'e1',
        "op" => "interrupt",
        "session" => "none",
        "status" => ["done", "interrupted"],
      )

      # Without an ID
      result = client.write(msg)
      client.write({'op' => 'interrupt'})
      expect(client.read).to eql(
        'id' => 'e1',
        "op" => "interrupt",
        "session" => "none",
        "status" => ["done", "interrupted"],
      )
    end
  end

  context "when tracing is enabled (it's the default)" do
    it "automatically watches stuff" do
      msg = { 'id' => 'e1', 'op' => 'eval', 'file' => '/tmp/ex.rb', 'line' => 0, 'code' => <<-RUBY }
      def foo(a, b)
        c = a + b
        c / 2.0
      end
      RUBY

      response = {'session'=>'none', 'status'=>['done'], 'id' => 'e1', 'value' => ':foo'}
      expect(client.rpc(msg)).to eql(response)

      expect(client.rpc(msg.merge('code' => 'foo(9, 5)', 'line' => 4))).to eql(
        response.merge('value' => '7.0')
      )

      # This will re-use the binding for the method because of the line
      expect(client.rpc(msg.merge('code' => '[a, b, c]', 'line' => 2))).to eql(
        response.merge('value' => '[9, 5, 14]')
      )

      # And we can also unwatch
      res = client
        .rpc('op' => 'unwatch', 'watch_id' => '/tmp/ex.rb:1')
        .tap { |x| x.delete('id') }
      # res = client.read!
      expect(res).to eql('op' => 'unwatch', 'session' => 'none', "status"=>["done"])

      # And the binding is gone
      msg.update('code' => 'a', 'id' => 'eval_3')
      expect(client.rpc(msg)['ex']).to match(/undefined local variable or method `a'/)

    end
  end

  def define_watch_point!(client, default_ids: false)
    code = <<-RUBY
      def stopped_call
        variable = 40
        NREPL.watch! binding#{", 'some-id'" unless default_ids}
        NREPL.watch! binding#{", 'after-method'" unless default_ids}
        variable + 2
      end
    RUBY

    # Defines a "watch" point
    eval_msg = {
      'op' => 'eval',
      'code' => code,
      'id' => 'eval_watch',
      "file" => "/tmp/some_file.rb",
      "line" => 20,
    }
    r = client.rpc(eval_msg)
    r
  end

  def eval_to_watch_method!(client)
    eval_msg = {
      'op' => 'eval',
      'code' => 'stopped_call()',
      'id' => 'eval_to_watch'
    }
    client.write(eval_msg)
    [client.read, client.read]
  end
end
