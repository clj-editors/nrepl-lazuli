# nREPL-Lazuli

An nREPL server for Ruby, to be used with Lazuli plug-in

## Usage

There are multiple ways of using nREPL-Lazuli in your code. First, if you have a
code that keeps running - like a Sinatra webserver, or Rails, or a microservice
that connects to Kafka, RabbitMQ, or SQS, the only thing you need to do is to
add some code to load the server before running your process (or after too - it
doesn't matter that much the order, but usually running before get better
results):

```ruby
require 'nrepl-lazuli' # Might not be needed if using Bundler
Thread.new { NREPL::Server.start() }

require 'sinatra'
# ... your code here...
```

If you're using Rails, you can add an initializer like `config/initializers/nrepl.rb`:

```ruby
if defined?(Rails::Server)
  Thread.new { NREPL::Server.start() }
  # If you want to log more stuff into the console, add the lines below too:
  sleep 1
  # This is for older Rails apps:
  Rails.logger = ActiveSupport::Logger.new($stdout)

  # This is for newer Rails apps:
  if( Rails.logger.class == ActiveSupport::BroadcastLogger )
    logger = ActiveSupport::Logger.new($stdout)
    logger.formatter = Rails.logger.broadcasts[-1].formatter
    Rails.logger.broadcasts << logger
  end
end
```

### Usage if your app doesn't keep running, like tests

You might want to use nREPL-Lazuli to help you write tests, for example. There's
a work-in-progress to make this experience better, but for now, one way to do it
is to use like `binding.pry` - stopping the process in the middle of something,
and then connecting the editor to nREPL. To do that, you can use this API:

```ruby
# Suppose you have a spec like this:

it "calculates the sum between two numbers" do
  a = 10
  b = 20
  # Add this line:
  NREPL::Server.bind!(binding)
end
```

This code above will stop the app, exactly the same as `binding.pry` would do,
but open an nREPL server at that position with the binding saved to the server,
so that you can evaluate code in that context, even with local variables
pre-defined.

## Special thanks

Delon Newman, for the proof-of-concept of using a nREPL server in Ruby: contact@delonnewman.name
