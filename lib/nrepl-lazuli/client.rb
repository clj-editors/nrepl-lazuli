# frozen_string_literal: true

require_relative 'bencode'
require 'timeout'

module NREPL
  class Client
    attr_reader :host, :port

    def initialize(port: DEFAULT_PORT, host: DEFAULT_HOST)
      @port       = port
      @host       = host
      @socket = TCPSocket.new(@host, @port)
      @bencode = BEncode.new(@socket)
      @last_id = 0
    end

    def stop
      unless closed?
        @socket.close
        @socket = nil
        @bencode = nil
      end
    end

    def rpc(msg)
      id = msg["id"] || msg[:id]
      if(!id)
        @last_id += 1
        id = "autogen_#@last_id"
        msg = msg.merge('id' => id)
      end

      write(msg)
      Timeout.timeout(10) do
        loop do
          ret = read
          if(ret["id"] == id)
            break ret
          end
        end
      end
    end

    def write(msg)
      @socket.write(BEncode.encode(msg))
      @socket.flush
    end

    def read
      @bencode.parse!
    end

    def register_session
      write('op' => 'clone')
      msg = read!

      raise ArgumentError, "failed to create session" unless msg['new_session']
      msg['new_session']
    end

    def closed?
      @socket.nil?
    end
  end
end
