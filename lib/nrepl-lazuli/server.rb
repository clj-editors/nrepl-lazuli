# frozen_string_literal: true
#
# A Ruby port of ogion https://gitlab.com/technomancy/ogion &
# https://github.com/borkdude/nrepl-server/blob/master/src/borkdude/nrepl_server.clj

require 'socket'
require_relative 'bencode'
require_relative 'connection'
require_relative 'fake_stdout'

module NREPL
  class Server
    attr_reader :debug, :port, :host
    alias debug? debug

    def self.spawn(args = {})
      t = Thread.new {
        new(**args).start
      }
      sleep 0.1
      t[:nrepl_server]
    end

    def self.start(**kwargs)
      new(**kwargs).start
    end

    def self.bind!(binding, **kwargs)
      new(**kwargs.merge(binding: binding)).start
    end

    def initialize(
      port: DEFAULT_PORT,
      host: DEFAULT_HOST,
      debug: false,
      binding: nil,
      pwd: Dir.pwd,
      tracing: true
    )
      @port  = port
      @pwd = pwd
      @host  = host
      @debug = debug
      @connections = Set.new
      @binding = binding
      loc = caller_locations.reject { |x| x.absolute_path =~ /nrepl\-lazuli/ }[0]
      loc = caller_locations[0] if(!loc)
      path = loc.absolute_path
      line = loc.lineno - 1
      @bindings = {path => {line => {"NREPL::Server.start" => binding}}}
      @bindings_by_id = {
        "NREPL::Server.start" => {
          binding: binding,
          file: path,
          row: line
        }
      }
      @tracing = tracing
      Thread.current[:nrepl_server] = self
      NREPL.class_variable_set(:@@connections, @connections)
      NREPL.class_variable_set(:@@bindings, @bindings)
      NREPL.class_variable_set(:@@bindings_by_id, @bindings_by_id)
    end

    private def record_port
      File.open(PORT_FILENAME, 'w+') do |f|
        f.write(port)
      end
    end

    def start
      puts "nREPL server started on port #{port} on host #{host} - nrepl://#{host}:#{port}"
      puts "Running in debug mode" if debug?
      record_port

      @old_out, @old_err = $stdout, $stderr
      $stdout = FakeStdout.new(@connections, STDOUT, "out")
      $stderr = FakeStdout.new(@connections, STDERR, "err")
      auto_create_bindings! if @tracing

      Signal.trap("INT") { stop }
      Signal.trap("TERM") { stop }

      @socket = TCPServer.new(host, port)
      loop do
        break if @socket.closed?
        Thread.start(@socket.accept) do |client|
          connection = Connection.new(
            client,
            debug: debug?,
            binding: @binding,
            bindings_by_id: @bindings_by_id,
            bindings: @bindings
          )
          @connections << connection
          connection.treat_messages!
          @connections.delete(connection)
        end
      end
    rescue IOError
      puts "Server closed" if debug?
    ensure
      File.unlink(PORT_FILENAME)
    end

    def auto_create_bindings!
      dir_regex = Regexp.new("^#{Regexp.escape(@pwd)}")
      @call_trace = TracePoint.new(:class, :call) do |tp|
        path = tp.path
        next if tp.path =~ /^(\<|.eval)/
        path = File.join(@pwd, path) if File.dirname(path) == '.'
        id = "#{path}:#{tp.lineno}"
        b = tp.binding
        @bindings_by_id[id] = {binding: b, file: path, row: tp.lineno-1}
        @bindings[path] ||= {}
        @bindings[path][tp.lineno-1] ||= {}
        @bindings[path][tp.lineno-1][id] = b
        if path =~ dir_regex
          @connections.each do |connection|
            connection.send_msg(
              'op' => 'hit_auto_watch',
              'file' => path,
              'line' => tp.lineno-1,
              'status' => ['done']
            )
          end
        end
      end
      @call_trace.enable

      @ex_trace = TracePoint.new(:raise) do |tp|
        e = tp.raised_exception
        @@last_exception = [:error, e.inspect.sub(/\>$/, ' stack=' + e.backtrace.inspect+'>')]
      end
      @ex_trace.enable
    end

    def stop
      @connections = []
      $stdout, $stderr = @old_out, @old_err
      @socket.close
    end
  end

  # Sorry, no other way...
  module ThreadPatch
    def initialize(*args, &b)
      @parent = Thread.current
      super
    end

    def parent
      @parent
    end
  end

  Thread.prepend(ThreadPatch)

  # Also...
  module MethodLocationFixer
    def __lazuli_source_location
      @__lazuli_source_location || source_location
    end
  end

  module DefinitionFixer
    @@definitions = {}

    def __lazuli_source_location(method)
      final_loc = nil
      loc = ancestors.each do |klass|
        loc = (klass.instance_variable_get(:@__lazuli_methods) || {})[method]
        if loc
          final_loc = loc
          break
        end
      end

      if(final_loc && File.exist?(final_loc[0]))
        final_loc
      else
        instance_method(method).source_location
      end
    end

    def method_added(method_name)
      return if method_name == :__lazuli_source_location
      pwd = Dir.pwd
      path = caller.select { |x| x.start_with?(pwd) }[0]
      if path
        (file, row) = path.split(/:/)

        known = instance_variable_get(:@__lazuli_methods)
        if !known
          known = {}
          instance_variable_set(:@__lazuli_methods, known)
        end
        known[method_name] = [file, row.to_i]
      end
    end

    Module.prepend(DefinitionFixer)
  end
end
