# frozen_string_literal: true

module NREPL
  VERSION = '0.1.0'
  DEFAULT_PORT  = 7888
  DEFAULT_HOST  = '127.0.0.1'
  PORT_FILENAME = '.nrepl-port'

  require_relative 'nrepl-lazuli/server'
  @@bindings = {}
  @@bindings_by_id = {}
  @@connections = Set.new

  def self.watch!(binding, id=nil)
    loc = caller_locations[0]
    file = loc.path
    row = loc.lineno
    id ||= "#{file}:#{row}"

    @@bindings_by_id[id] = {binding: binding, file: file, row: loc.lineno-1}
    @@bindings[file] ||= {}
    @@bindings[file][loc.lineno-1] ||= {}
    @@bindings[file][loc.lineno-1][id] = binding

    @@connections.each do |connection|
      connection.send_msg(
        'op' => 'hit_watch',
        'id' => id,
        'file' => file,
        'line' => row - 1,
        'status' => ['done']
      )
    end
    nil
  end
end
