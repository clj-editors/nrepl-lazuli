Gem::Specification.new do |s|
  s.name        = "nrepl-lazuli"
  s.version     = "0.4.0"
  s.summary     = "A Ruby nREPL server"
  s.description = "A Ruby nREPL server, made to be used with Lazuli plug-in (but can be used with any nREPL client too)"
  s.authors     = ["Maurício Szabo"]
  s.email       = "mauricio@szabo.link"
  s.files       = ["lib/nrepl-lazuli.rb", "lib/nrepl-lazuli/server.rb", "lib/nrepl-lazuli/connection.rb", "lib/nrepl-lazuli/fake_stdout.rb"]
  s.homepage    = "https://gitlab.com/clj-editors/nrepl-lazuli"
  s.license     = "MIT"
  s.metadata    = { "source_code_uri" => "https://gitlab.com/clj-editors/nrepl-lazuli" }
end
